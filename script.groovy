def buildJar() {
    echo "building the application..."
    sh ('npm install') // Install project dependencies
   
} 

def buildImage() {
    echo "building the docker image..."
    withCredentials([usernamePassword(credentialsId: 'docker-hub-repo', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
        sh ('docker build -t hendrick23/my-repo:api_node-3.0 .')
        sh ("echo $PASS | docker login -u $USER --password-stdin")
        sh ('docker push hendrick23/my-repo:api_node-3.0')
    }
} 
def test() {
    echo "Running tests..."
    sh ('npm test') // Run tests using the configured test command
    echo "Running tests baby..."
}

def deployApp() {
    def dockerCmd = 'docker run -p 3000:3000 -d hendrick23/my-repo:api_node-3.0'
    sshagent(['ec2-server-key']) {
        sh "ssh -o StrictHostKeyChecking=no ec2-user@16.171.62.12 $dockerCmd"
}
} 

return this
